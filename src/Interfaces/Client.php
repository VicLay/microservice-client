<?php

namespace Viclay\MicroserviceClient\Interfaces;

interface Client
{
    public function get(string $uri, array $options = []): Response;

//    public function post(): Response;
//    public function put(): Response;
//    public function patch(): Response;
//    public function delete(): Response;


    public function send(Request $request): Response;
}
