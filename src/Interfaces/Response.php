<?php

namespace Viclay\MicroserviceClient\Interfaces;

use Psr\Http\Message\ResponseInterface;

interface Response
{
    public function parseHttpResponse(ResponseInterface $response):void;

    public function getData(): ?array;

    public function getStatusCode(): int;

    public function getContentType(): string;
}
