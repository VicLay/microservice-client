<?php

namespace Viclay\MicroserviceClient\Interfaces;

interface Request
{
    public function getMethod();

    public function getUri();

    public function getOptions();

    public function makeResponse();

}
