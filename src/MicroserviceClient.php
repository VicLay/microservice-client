<?php

declare(strict_types=1);

namespace Viclay\MicroserviceClient;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\GuzzleException;
use Viclay\MicroserviceClient\Exceptions\MicroserviceClientException;
use Viclay\MicroserviceClient\Interfaces\Client;
use Viclay\MicroserviceClient\Interfaces\Request as RequestInterface;
use Viclay\MicroserviceClient\Interfaces\Response as ResponseInterface;
use Viclay\MicroserviceClient\Http\Request;

class MicroserviceClient implements Client
{
    const TIMEOUT = 10;
    const ACCEPT_TYPE = 'application/json';
    const ENCODING = 'gzip';

    protected GuzzleClient $client;

    public function __construct(string $baseUri = '')
    {
        $config = [
            'base_uri' => $baseUri,
            'headers' => [
                'Accept-Encoding' => self::ENCODING,
                'Accept' => self::ACCEPT_TYPE,
            ],
            'timeout' => self::TIMEOUT,
        ];

        $this->client = new GuzzleClient($config);
    }

    /**
     * @throws MicroserviceClientException
     */
    public function get(string $uri, array $options = []): ResponseInterface
    {
        $request = new Request('GET', $uri, $options);

        return $this->send($request);
    }


    /**
     * @throws MicroserviceClientException
     */
    public function send(RequestInterface $request): ResponseInterface
    {
        try {
            $httpResponse = $this->client->request(
                $request->getMethod(),
                $request->getUri(),
                $request->getOptions()
            );
        } catch (GuzzleException $e) {
            throw new MicroserviceClientException(
                $e->getMessage(),
                $e->getCode(),
                $e->getPrevious()
            );
        }

        $response = $request->makeResponse();
        $response->parseHttpResponse($httpResponse);

        return $response;
    }
}
