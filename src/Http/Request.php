<?php

namespace Viclay\MicroserviceClient\Http;

use Viclay\MicroserviceClient\Interfaces\Request as RequestInterface;

class Request implements RequestInterface
{
    public function getMethod()
    {
        return $this->method;
    }

    public function getUri()
    {
        return $this->uri;
    }

    public function getOptions()
    {
        return $this->option;
    }

    public function makeResponse(): \Viclay\MicroserviceClient\Interfaces\Response
    {
        return new Response(clone $this);
    }

    public function __construct(protected $method,protected  $uri,protected  $option)
    {
    }
}

