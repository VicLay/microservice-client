<?php

namespace Viclay\MicroserviceClient\Http;

use Psr\Http\Message\ResponseInterface as HttpResponseInterface;
use Viclay\MicroserviceClient\Interfaces\Request;
use Viclay\MicroserviceClient\Interfaces\Response as ResponseInterface;

class Response implements ResponseInterface
{
    protected const CONTENT_TYPE = 'application/json';

    protected mixed $data = [];
    public function __construct(
        protected Request $request,
        protected ?HttpResponseInterface $response = null
    ) {
    }

    public function parseHttpResponse(HttpResponseInterface $response): void
    {
        $this->response = $response;
        $this->data = $response->getBody()->getContents();
    }

    public function getData(): ?array
    {
        if ($this->getContentType() === self::CONTENT_TYPE) {

            return json_decode($this->data, true);
        }

        return null;
    }

    public function getStatusCode(): int
    {
        return $this->response->getStatusCode();
    }

    public function getContentType(): string
    {
        return $this->response->getHeader('Content-Type')[0];
    }
}
